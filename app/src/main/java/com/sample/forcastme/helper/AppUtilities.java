package com.sample.forcastme.helper;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Created by Ajijul Mondal on 27-Jan-16.
 */
public class AppUtilities {
    private static Context context = null;

    /**
     * Recently set context will be returned.
     * If not set it from current class it will
     * be null.
     *
     * @return Context
     */
    public static final Context getContext() {
        return AppUtilities.context;
    }

    /**
     * First set context from every activity
     * before use any static method of AppUtils class.
     *
     * @param ctx
     */
    public static final void setContext(Context ctx) {
        AppUtilities.context = ctx;
    }

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(int res) {
        Context _ctx = getContext();
        if (null != _ctx) {
            try {
                return _ctx.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(Context context, int res) {
        if (null != context) {
            try {
                return context.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivity(Class cls) {
        Context _ctx = getContext();
        if (null != _ctx) {
            Intent intent = new Intent(_ctx, cls);
            _ctx.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Log.e("AppUtilities", "Context Null");
        }
    }


    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivityWithSingleExtra(Class cls, String stringExtra) {
        Context _ctx = getContext();
        if (null != _ctx) {
            Intent intent = new Intent(_ctx, cls);
            intent.putExtra("extra", stringExtra);
            _ctx.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Log.e("AppUtilities", "Context Null");
        }
    }

    public static int getStatusBarHeight() {

        Rect rect = new Rect();
        Window win = ((Activity) getContext()).getWindow();
        win.getDecorView().

                getWindowVisibleDisplayFrame(rect);

        int statusBarHeight = rect.top;
        int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        if (statusBarHeight == 0) {
            statusBarHeight = 40;
        }
        return statusBarHeight;
    }


    public static void setSnackNotification(String message, View parentView, int backgroundColor, int textColor,
                                            Boolean isActionBtnPresent, String actionText,
                                            int actionBtnTextColor) {
        if (parentView != null) {

            final Snackbar snackbar = Snackbar
                    .make(parentView, message, Snackbar.LENGTH_LONG);

            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView mySnackText = (TextView)
                    snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            mySnackText.setTextColor(textColor);
            mySnackText.setGravity(Gravity.CENTER);

            if (isActionBtnPresent) {

                TextView mySnackActionText = (TextView)
                        snackbarView.findViewById(android.support.design.R.id.snackbar_action);
                mySnackActionText.setTextColor(actionBtnTextColor);
                mySnackActionText.setTypeface(Typeface.DEFAULT);
                snackbar.setAction(actionText, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });

            }


            snackbar.show();
        } else {
            Log.e("AppUtilities", "SnackBer Null");

        }
    }

    public static final void hideSoftInputMode(Context context, final EditText et) {
        Context _ctx = context;
        if (null != _ctx) {
            InputMethodManager im = (InputMethodManager) _ctx.getSystemService(_ctx.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } else {
            Log.e("AppUtilities", "Context Null");
        }
    }


    public static boolean isMyServiceRunning(Context _context, String serviceNameWithPackageName) {
        ActivityManager manager = (ActivityManager) _context.getSystemService(_context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceNameWithPackageName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static byte[] encryptOrDecryptKeyGenerator(String myEncryptionKey) {
        byte[] key = null;
        try {

            byte[] keyStart = myEncryptionKey.getBytes();
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = new SecureRandom();
            /*SecureRandom.getInstance("SHA1PRNG");*/
            sr.setSeed(keyStart);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            key = skey.getEncoded();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;

    }
}
