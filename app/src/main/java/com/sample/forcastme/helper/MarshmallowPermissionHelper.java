package com.sample.forcastme.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.sample.forcastme.R;


/**
 * Created by Ajijul Mondal on 15-Feb-16.
 */
public class MarshmallowPermissionHelper {

    /*Permission Checking for Location*/

    public static boolean getLocationPermission(final Fragment fragment, final Activity activity, final int REQUEST_CODE) {

        Context context = null;
        if (fragment != null)
            context = fragment.getContext();
        else
            context = activity;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {

                    android.support.v7.app.AlertDialog.Builder builder =
                            new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Permission Required");
                    builder.setMessage("Location Permission Required. " +
                            "You have to grant this permission in order to use this feature.");
                    builder.setPositiveButton("Got it",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (fragment != null)
                                        fragment.requestPermissions(
                                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE);

                                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        activity.requestPermissions(
                                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE);
                                    }

                                }
                            });
                    builder.show();


                } else {


                    if (fragment != null)
                        fragment.requestPermissions(
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_CODE);

                    else activity.requestPermissions(
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_CODE);


                }
                return false;
            } else
                return true;
        } else {
            return true;
        }

    }

    /*Permission Checking for Storage And Camera*/
    public static boolean getStorageAndCameraPermission(final Context context,
                                                        final Fragment fragment,
                                                        final Activity activity,
                                                        final int REQUEST_CODE) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {


                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    android.support.v7.app.AlertDialog.Builder builder =
                            new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Permission Required");
                    builder.setMessage("Camera Permission and Read External Storage Permission Required. " +
                            "You have to grant both permission in order to use this feature.");
                    builder.setPositiveButton("Got it",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (fragment != null) {
                                        fragment.requestPermissions(
                                                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                REQUEST_CODE);

                                    } else if (null != activity) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            activity.requestPermissions(
                                                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                    REQUEST_CODE);
                                        }
                                    }

                                }
                            });
                    builder.show();

                    return false;
                } else {
                    if (fragment != null) {
                        fragment.requestPermissions(
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE);

                    } else if (null != activity) {
                        activity.requestPermissions(
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE);
                    }


                }
                return false;
            } else
                return true;
        } else
            return true;


    }

     /*Permission Checking for Location*/

    public static boolean getPhonePermission(final Fragment fragment, final Activity activity,
                                             final int REQUEST_CODE) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CALL_PHONE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.READ_PHONE_STATE)) {

                    android.support.v7.app.AlertDialog.Builder builder =
                            new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Permission Required");
                    builder.setMessage("Call Permission Required. " +
                            "You have to grant this permission in order to use this feature.");
                    builder.setPositiveButton("Got it",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (fragment != null)
                                        fragment.requestPermissions(
                                                new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE},
                                                REQUEST_CODE);

                                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        activity.requestPermissions(
                                                new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE},
                                                REQUEST_CODE);
                                    }

                                }
                            });
                    builder.show();


                } else {


                    if (fragment != null)
                        fragment.requestPermissions(
                                new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE},
                                REQUEST_CODE);

                    else activity.requestPermissions(
                            new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE},
                            REQUEST_CODE);


                }
                return false;
            } else
                return true;
        } else {
            return true;
        }

    }
}
