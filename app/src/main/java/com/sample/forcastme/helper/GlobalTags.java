package com.sample.forcastme.helper;

public interface GlobalTags {
    String LAT = "lat";
    String LONG = "long";
    int APP_LOCATION_PERMISSION = 101;
    String CURRENT_LAT = "lat";
    String CURRENT_LNG = "long";
}
