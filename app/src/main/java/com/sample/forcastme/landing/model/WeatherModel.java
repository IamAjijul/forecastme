package com.sample.forcastme.landing.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WeatherModel  {
    String  description;
    String  main;

    public WeatherModel(String description, String main) {
        this.description = description;
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }
}
