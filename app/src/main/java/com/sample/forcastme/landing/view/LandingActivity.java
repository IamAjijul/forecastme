package com.sample.forcastme.landing.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.saifzone.saifviolations.retrofit.ApiServices;
import com.saifzone.saifviolations.retrofit.RequestHandler;
import com.saifzone.saifviolations.retrofit.RetroInstance;
import com.saifzone.saifviolations.retrofit.SetOnResponseListener;
import com.sample.forcastme.R;
import com.sample.forcastme.helper.GlobalTags;
import com.sample.forcastme.landing.PagerAdapter;
import com.sample.forcastme.landing.model.LandingDataModel;
import com.sample.forcastme.landing.model.ListModel;
import com.sample.forcastme.landing.model.MainModel;
import com.sample.forcastme.landing.model.custom_model.DayModel;
import com.sample.forcastme.landing.model.custom_model.TempModel;
import com.sample.forcastme.utilities.NetworkUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.paperdb.Paper;
import retrofit2.Response;

public class LandingActivity extends AppCompatActivity {

    private ArrayList<ListModel> list = new ArrayList<>();
    private ViewPager viewPager;
    private String[] title;
    private TabLayout tabLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_activity);
        // Find the view pager that will allow the user to swipe between fragments
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        // Create an adapter that knows which fragment should be shown on each page


        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(findViewById(R.id.llParent),
                    "No Internet Connection", Snackbar.LENGTH_LONG).show();
            return;
        }
        ApiServices apiServices = RetroInstance.Companion.getInstance().create(ApiServices.class);

        apiServices.getReport(Paper.book().read(GlobalTags.CURRENT_LAT,"25.286806359624656")
                ,Paper.book().read(GlobalTags.CURRENT_LNG,"55.35055340866414"),"e5778733cda6338d0a4b9b71f54ebbb3").enqueue(new RequestHandler<LandingDataModel>(new SetOnResponseListener() {
            @Override
            public <T> void onSuccess(@org.jetbrains.annotations.Nullable Response<T> response) {

                LandingDataModel dataModel =(LandingDataModel) response.body();
                customizeArray(dataModel);

            }

            @Override
            public void onError(@org.jetbrains.annotations.Nullable String error) {

            }
        },this));
    }

    private void customizeArray(LandingDataModel dataModel) {
        list= dataModel.list;
        ArrayList<DayModel> dayModels = new ArrayList<>();
        ArrayList<TempModel> tempModels = new ArrayList<>();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("EE hh a");
        String previousDay = "";
        long previousTemp = 100;
        for (int i =0;i<list.size();i++){
            ListModel listModel = list.get(i);

            String dateString = listModel.getDt_txt();
            Date date = null;
            try {
                date = format1.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String dayNameWithTime = format2.format(date);
            String dayName = getDayFromDateString(dateString,"yyyy-MM-dd HH:MM:ss");
            String onlyTime = dayNameWithTime.split( " ")[1]+ dayNameWithTime.split( " ")[2];
            MainModel mainModel = listModel.getMain();
            long minTemp = Math.round(Float.parseFloat(mainModel.getTemp_min()) - 273.15);

            if(i==0)
                previousDay = dayName;


            if(!previousDay.equals(dayName) ){
            String city = dataModel.city.getName();
            DayModel model = new DayModel(previousDay,city,tempModels,previousDay+" low "+String.valueOf(previousTemp) + (char) 0x00B0);
            dayModels.add(model);
                tempModels = new ArrayList<>();
                previousDay = dayName;
                previousTemp = 100;
            }
            if(previousTemp>=minTemp)
                previousTemp = minTemp;
            String temp = String.valueOf(Math.round(Float.parseFloat(mainModel.getTemp()) - 273.15)) + (char) 0x00B0;
            TempModel tempModel = new TempModel(temp,onlyTime,listModel.getWeather().get(0).getDescription(),
                    mainModel.getHumidity(),listModel.getWind().getSpeed()+" KpH",mainModel.getPressure(),mainModel.getSea_level());
            tempModels.add(tempModel);
            if( i ==(list.size()-1)){
                String city = dataModel.city.getName();
                DayModel model = new DayModel(previousDay,city,tempModels,previousDay+" low "+String.valueOf(previousTemp) + (char) 0x00B0);
                dayModels.add(model);
                tempModels = new ArrayList<>();
                previousDay = dayName;

            }

        }

        PagerAdapter adapter = new PagerAdapter(LandingActivity.this, getSupportFragmentManager(),dayModels);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager

        tabLayout.setupWithViewPager(viewPager);
    }

    public static String getDayFromDateString(String stringDate,String dateTimeFormat)
    {


        String[] daysArray = new String[] {"Fri","Sat","Sun","Mon","Tue","Wed","Thu"};
        String day = "";

        int dayOfWeek =0;
        //dateTimeFormat = yyyy-MM-dd HH:mm:ss
        SimpleDateFormat formatter = new SimpleDateFormat(dateTimeFormat);
        Date date;
        try {
            date = formatter.parse(stringDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK)-1;
            day = daysArray[dayOfWeek];
        } catch (Exception e) {
            e.printStackTrace();
        }

        return day;
    }
}
