package com.sample.forcastme.landing.model;

import java.util.ArrayList;

public class LandingDataModel {
    public String message;
    public ArrayList<ListModel> list = new ArrayList<>();
    public CityModel city;

    public LandingDataModel(String message, ArrayList<ListModel> list, CityModel city) {
        this.message = message;
        this.list = list;
        this.city = city;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ListModel> getList() {
        return list;
    }

    public void setList(ArrayList<ListModel> list) {
        this.list = list;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }
}
