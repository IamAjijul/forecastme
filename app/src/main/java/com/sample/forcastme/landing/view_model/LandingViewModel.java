package com.sample.forcastme.landing.view_model;

import android.app.Activity;
import android.support.annotation.NonNull;

public class LandingViewModel {
    private Activity activity;
    private int position;

    public LandingViewModel(Activity activity, int position) {
        this.activity = activity;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    /**
     * On City clicked
     *
     * @param lat latitude
     * @param lon longitude
     */
    public void onCityClicked(@NonNull String lat,@NonNull String lon ) {


    }

    /**
     * @param position         Tab Position
     */
    public void onTabChange(@NonNull int position) {


    }
}
