package com.sample.forcastme.landing;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sample.forcastme.landing.model.custom_model.DayModel;
import com.sample.forcastme.landing.view.WeatherReportFragment;

import java.util.ArrayList;

public class PagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private ArrayList<DayModel> dayModels;

    public PagerAdapter(Context context, FragmentManager fm, ArrayList<DayModel> dayModels) {
        super(fm);
       this. mContext = context;
       this. dayModels = dayModels;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        return  WeatherReportFragment.newInstance(dayModels.get(position),position);

    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return dayModels.size();
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {


        return dayModels.get(position).getDayName();
    }

}