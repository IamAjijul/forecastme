package com.sample.forcastme.landing.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ListModel {
    String dt_txt;
    MainModel main ;
    ArrayList<WeatherModel> weather = new ArrayList<>();
    WindModel wind ;

    public ListModel(String dt_txt, MainModel main, ArrayList<WeatherModel> weather, WindModel wind) {
        this.dt_txt = dt_txt;
        this.main = main;
        this.weather = weather;
        this.wind = wind;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public MainModel getMain() {
        return main;
    }

    public void setMain(MainModel main) {
        this.main = main;
    }

    public ArrayList<WeatherModel> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<WeatherModel> weather) {
        this.weather = weather;
    }

    public WindModel getWind() {
        return wind;
    }

    public void setWind(WindModel wind) {
        this.wind = wind;
    }
}
