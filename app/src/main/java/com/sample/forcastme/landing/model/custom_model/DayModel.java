package com.sample.forcastme.landing.model.custom_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DayModel implements Parcelable {
    String city;
    ArrayList<TempModel> tempModels;
    String temp_min;
    String dayName;

    public DayModel(String dayName,String city, ArrayList<TempModel> tempModels, String temp_min) {
        this.city = city;
        this.tempModels = tempModels;
        this.temp_min = temp_min;
        this.dayName = dayName;
    }

    protected DayModel(Parcel in) {
        city = in.readString();
        temp_min = in.readString();
        dayName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeString(temp_min);
        dest.writeString(dayName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DayModel> CREATOR = new Creator<DayModel>() {
        @Override
        public DayModel createFromParcel(Parcel in) {
            return new DayModel(in);
        }

        @Override
        public DayModel[] newArray(int size) {
            return new DayModel[size];
        }
    };

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public ArrayList<TempModel> getTempModels() {
        return tempModels;
    }

    public void setTempModels(ArrayList<TempModel> tempModels) {
        this.tempModels = tempModels;
    }
}
