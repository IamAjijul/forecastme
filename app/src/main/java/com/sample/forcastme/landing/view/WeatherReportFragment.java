package com.sample.forcastme.landing.view;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.sample.forcastme.R;
import com.sample.forcastme.databinding.FragmentLayoutBinding;
import com.sample.forcastme.landing.model.custom_model.DayModel;
import com.sample.forcastme.landing.model.custom_model.TempModel;
import com.sample.forcastme.landing.view_model.LandingViewModel;

import java.util.ArrayList;
import java.util.Collections;

public class WeatherReportFragment extends Fragment {
    private DayModel dayModel;
    private int position;
    private LineChart mpLineChart;
    private LinearLayout llFooter;

    public static WeatherReportFragment newInstance(DayModel dayModel, int position) {

        Bundle args = new Bundle();
        args.putParcelable("dayModel", dayModel);
        args.putInt("position", position);
        WeatherReportFragment fragment = new WeatherReportFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dayModel = getArguments().getParcelable("dayModel");
        position = getArguments().getInt("position");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentLayoutBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_layout, container, false);
        View view = binding.getRoot();
        mpLineChart = (LineChart) view.findViewById(R.id.mpLineChart);
        llFooter = (LinearLayout) view.findViewById(R.id.llFooter);
        mpLineChart.getAxisLeft().setDrawGridLines(false);
        mpLineChart.getAxisLeft().setEnabled(false);
        mpLineChart.getAxisRight().setEnabled(false);
        mpLineChart.getAxisRight().setDrawGridLines(false);
        mpLineChart.getXAxis().setDrawGridLines(false);
        mpLineChart.getLegend().setEnabled(false);
        mpLineChart.getXAxis().setTextColor(Color.WHITE);
        mpLineChart.setDescription("");
        mpLineChart.setPinchZoom(false);
        mpLineChart.setDoubleTapToZoomEnabled(false);
        mpLineChart.setTouchEnabled(false);
        mpLineChart.setVisibleXRangeMaximum(100f);
        generateChart();
        //here data must be an instance of the class MarsDataProvider
        binding.setModel(dayModel);
        binding.setTempModel(dayModel.getTempModels().get(0));
        binding.setViewModel(new LandingViewModel(getActivity(), position));
        return view;
    }

    private void generateChart() {
        ArrayList<TempModel> tempModels = dayModel.getTempModels();
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> xAXES = new ArrayList<>();
        llFooter.setWeightSum(tempModels.size());
        for (int i = 0; i < tempModels.size(); i++) {
            entries.add(new Entry(Float.parseFloat(tempModels.get(i).getTemp().replace("\u00B0", "")), i));
            TextView textView = new TextView(getContext());
            TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);
            if (i == 0 && position == 0)
                textView.setText("Now");
            else {
                textView.setText(tempModels.get(i).getTime());
                textView.setTextSize(10f);
            }

            textView.setLayoutParams(params);
            textView.setTextColor(Color.WHITE);
            llFooter.addView(textView);
            llFooter.invalidate();
            xAXES.add(tempModels.get(i).getTemp());
        }
        LineDataSet lineDataSet = new LineDataSet(entries, "");
        lineDataSet.setDrawCircles(false);
        lineDataSet.setColor(Color.YELLOW);
        lineDataSet.setLineWidth(5f);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCubic(true);
        LineData lineData = new LineData(xAXES, lineDataSet);
        XAxis xAxis = mpLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        mpLineChart.setData(lineData);
    }
}

