package com.sample.forcastme.landing.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CityModel {
    String id;
    String country;
    String name;

    public CityModel(String id, String country, String name) {
        this.id = id;
        this.country = country;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
