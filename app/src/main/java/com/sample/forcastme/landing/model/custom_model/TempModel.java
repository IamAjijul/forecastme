package com.sample.forcastme.landing.model.custom_model;

import android.os.Parcel;
import android.os.Parcelable;

public class TempModel implements Parcelable {
    String temp;
    String time;
    String weather_description;
    String humidity;
    String wind;
    String pressure;
    String sea_level;

    public TempModel(String temp, String time, String weather_description, String humidity, String wind, String pressure, String sea_level) {
        this.temp = temp;
        this.time = time;
        this.weather_description = weather_description;
        this.humidity = humidity;
        this.wind = wind;
        this.pressure = pressure;
        this.sea_level = sea_level;
    }

    protected TempModel(Parcel in) {
        temp = in.readString();
        time = in.readString();
        weather_description = in.readString();
        humidity = in.readString();
        wind = in.readString();
        pressure = in.readString();
        sea_level = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(temp);
        dest.writeString(time);
        dest.writeString(weather_description);
        dest.writeString(humidity);
        dest.writeString(wind);
        dest.writeString(pressure);
        dest.writeString(sea_level);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TempModel> CREATOR = new Creator<TempModel>() {
        @Override
        public TempModel createFromParcel(Parcel in) {
            return new TempModel(in);
        }

        @Override
        public TempModel[] newArray(int size) {
            return new TempModel[size];
        }
    };

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWeather_description() {
        return weather_description;
    }

    public void setWeather_description(String weather_description) {
        this.weather_description = weather_description;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getSea_level() {
        return sea_level;
    }

    public void setSea_level(String sea_level) {
        this.sea_level = sea_level;
    }
}
