package com.sample.forcastme.splash;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sample.forcastme.R;
import com.sample.forcastme.helper.AppUtilities;
import com.sample.forcastme.helper.MarshmallowPermissionHelper;
import com.sample.forcastme.helper.GlobalTags;
import com.sample.forcastme.landing.view.LandingActivity;

import io.paperdb.Paper;

public class SplashActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient myGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ProgressDialog progressDialog;
    final long LOCATION_REQUEST_NORMAL_INTERVAL = 10000;
    final long LOCATION_REQUEST_FASTEST_INTERVAL = 5000;
    private final int DELAY_TIME = 2000;
    private Handler _handler;
    private Runnable _runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        checkPermission();
    }

    private void redirectWithDelay() {
        _handler = new Handler();
        _runnable = new Runnable() {
            @Override
            public void run() {
                AppUtilities.setContext(SplashActivity.this);
                AppUtilities.redirectActivity(LandingActivity.class);
                finish();
            }
        };
        _handler.postDelayed(_runnable, DELAY_TIME);

    }

    void checkPermission() {
        if (MarshmallowPermissionHelper.getLocationPermission(null, this,
                GlobalTags.APP_LOCATION_PERMISSION)) {
            setGoogleClientAndGetLocation();

        }
    }

    private void setGoogleClientAndGetLocation() {
        progressDialog = showProgressDialog();
        progressDialog.show();
        myGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();
        createLocationRequest();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case GlobalTags.APP_LOCATION_PERMISSION: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    setGoogleClientAndGetLocation();
                } else finish();
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("LocationUpdate", "ON-CONNECTED");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                myGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("LocationUpdate", "ON-FAILED");
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("LocationUpdate", "ON-FAILED");
        if (myGoogleApiClient != null)
            myGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LocationUpdate", "ON-LOCATION-CHANGED Lat : " + location.getLatitude()
                + " LONG : " + location.getLongitude());
        Paper.book().write(GlobalTags.CURRENT_LAT, "" + location.getLatitude());
        Paper.book().write(GlobalTags.CURRENT_LNG, "" + location.getLongitude());
        if (progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();

        redirectWithDelay();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_REQUEST_NORMAL_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }
    @Override
    public void onDestroy() {
        Log.e("LocationUpdate", "ON-DESTROY");
        if (myGoogleApiClient != null)
            myGoogleApiClient.disconnect();
        super.onDestroy();
    }
    private ProgressDialog showProgressDialog()
    {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle(getString(R.string.gettingLocation));
        pd.setCancelable(false);
        pd.setMessage(getString(R.string.please_wait));
        pd.show();
        return pd;
    }

}
