package com.saifzone.saifviolations.retrofit

import com.sample.forcastme.landing.model.LandingDataModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {

    @GET("forecast")
    fun getReport(@Query("lat") lat: String, @Query("lon") long: String, @Query("appid") appid: String) : Call<LandingDataModel>


}