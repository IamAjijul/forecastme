package com.saifzone.saifviolations.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetroInstance private constructor() {

    companion object {
        private var mInstance: Retrofit? = null
        private val BASE_URL: String = "http://api.openweathermap.org/data/2.5/"
        private val mRequestTimeOut: Long = 180
        private val mReadTimeOut: Long = 180
        private val mWriteTimeOut: Long = 180

        @Synchronized
        fun getInstance(): Retrofit {
            if (mInstance == null) {
                mInstance = Retrofit.Builder().baseUrl(BASE_URL).client(getNetworkClient()
                ).addConverterFactory(GsonConverterFactory.create()).build()
            }
            return mInstance!!
        }

        private fun getNetworkClient(): OkHttpClient {
            val builder : OkHttpClient.Builder =  OkHttpClient().newBuilder()
                    .connectTimeout(mRequestTimeOut, TimeUnit.SECONDS)
                    .readTimeout(mReadTimeOut, TimeUnit.SECONDS)
                    .writeTimeout(mWriteTimeOut, TimeUnit.SECONDS)
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            return builder.build()
        }
    }

}