package com.saifzone.saifviolations.retrofit

import android.app.ProgressDialog
import android.content.Context
import android.support.annotation.Nullable
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.sample.forcastme.R

@Suppress("DEPRECATION")
class RequestHandler<T>() : Callback<T>{

    var listener : SetOnResponseListener? = null
    var mProgressBar : ProgressDialog? = null

    /**
     *  Provide mContext to show progress bar or provide null for without progress bar
     */
    constructor(listener : SetOnResponseListener, mContext : Context? ) : this(){
        this.listener = listener
        if(mContext != null){
            mProgressBar =  showProgressDialog(mContext)
        }
    }


    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        Log.e("Retrofit","OnResponse")
        if(mProgressBar!=null && mProgressBar!!.isShowing()){
         mProgressBar!!.dismiss()
     }
        listener!!.onSuccess(response)
    }

    override fun onFailure(call: Call<T>?, t: Throwable?) {
        Log.e("Retrofit","OnError")
        if(mProgressBar!=null && mProgressBar!!.isShowing()){
            mProgressBar!!.dismiss()
        }
        listener!!.onError(t!!.message)
    }

    private fun showProgressDialog(context: Context) : ProgressDialog
    {
        val pd = ProgressDialog(context)
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        pd.setTitle(context.getString(R.string.connecting))
        pd.setCancelable(false)
        pd.setMessage(context.getString(R.string.please_wait))
        pd.show()
        return pd
    }
}